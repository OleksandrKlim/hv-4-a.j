
const conteiner = document.createElement("ul");
document.body.prepend(conteiner);

fetch( "https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((res) => {
    res.map(({ episodeId, name, openingCrawl, characters }) => {
      try {
        let actors =" "
        const character = Promise.all(
          characters.map((url) => fetch(url).then((response) => response.json()))
        ).then((charactersList) => {
          charactersList.forEach(({ name }) => {
          actors +=" "+ name+"."
          });
          const filmList = document.createElement("ul");
          conteiner.appendChild(filmList);
          filmList.insertAdjacentHTML(
            "beforeend",
            ` <div class="movie">
            <p>Номер епізоду: ${episodeId}</p>
            <p>Назва фільму: ${name}</p>
            <p class = "characters" id = ${episodeId}">Персонажі:${actors}</p>
            <p>Короткий зміст : ${openingCrawl}</p>
            </div> `
          );
        });
      } catch (err) {
        console.error(err);
      }
    });
  })
  .catch((err) => {
    document.body.innerHTML = `<h1>Some Error: ${err.message}</h1>`;
  });
